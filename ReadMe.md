# myTerm

![][license]

This is **my Term**inal theme that I use in OS X/macOS. It's a modified version of the “Pro” theme. Different colors and blur on the background. It's dark (blue), and nice/calming for the eyes.


## Screenshots

![][scrap]

![][maingrp]

_The screenshot features [Archey X][archey]._


## Install

**`myTerm.terminal`** is the preference file for myTerm settings. Just double click on the file, or chose import from the Terminal preference window. And set to as your default theme.

You can also install (and make it default) via Terminal:

```bash
open "/path/to/myTerm.terminal"; \
sleep 2; \
defaults write com.apple.terminal "Default Window Settings" -string "myTerm"; \
defaults write com.apple.terminal "Startup Window Settings" -string "myTerm";
```

_The 2 sec sleep is to give it time to open and load, then it sets the theme to default._


## Terminal prefs

In case you want/need - here are a few xtra `defaults write`.

```bash
# ApplePersistence = 0;
defaults write com.apple.Terminal ApplePersistence -bool false

# FocusFollowsMouse = 0;
defaults write com.apple.Terminal FocusFollowsMouse -bool false

# NSQuitAlwaysKeepsWindows = 0;
defaults write com.apple.Terminal NSQuitAlwaysKeepsWindows -bool false

# SecureKeyboardEntry = 1;
defaults write com.apple.Terminal SecureKeyboardEntry -bool true

# utf-8
defaults write com.apple.terminal StringEncodings -array 4;
```

For misc xtra settings, ENV's, colors etc... Refer to my [dotfiles][df].


## Window groups

If you setup windows groups… Here are some useful prefs for that as well.

###### Example

Save a group as `maingrp`:

```bash
# "Startup Window Group" = maingrp;
defaults write com.apple.Terminal "Startup Window Group" -string maingrp

# NSUserKeyEquivalents =     {
#    maingrp = "@^1";
# };
defaults write com.apple.Terminal NSUserKeyEquivalents -dict-add "maingrp" "@^1"
```

That will make Terminal using the `maingrp` on launch. Shortcut for the window group is: `^⌘1`

<!-- Markdown: link & image dfn's -->
[license]: https://img.shields.io/badge/License-0BSD-789.svg?style=plastic "BSD Zero Clause License (SPDX: 0BSD)"
[scrap]: https://gitlab.com/iEFdev/myTerm/-/raw/main/images/myTerm.png "myTerm"
[maingrp]: https://gitlab.com/iEFdev/myTerm/-/raw/main/images/myTerm_maingrp.jpg "myTerm :: maingrp (Window group)"
[archey]: https://gitlab.com/iEFdev/Archey-X "Archey X"
[df]: https://gitlab.com/iEFdev/dotfiles/ "iEFdev/dotfiles"
